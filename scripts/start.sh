#!/bin/bash

# configure cassandra
/usr/local/bin/configure.sh

# start ssh server
echo cassandra | sudo -S /usr/sbin/sshd -D &

sleep 5

# start cassandra
# -e for start elassandra
# -f for start foreground
exec /opt/elassandra/bin/cassandra -e -f
